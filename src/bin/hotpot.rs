use std::path::PathBuf;

use anyhow::Result;
use clap::Parser;

use hotpot::{git::HpGit, mqtt::HpMqtt};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[arg(short, long, help = "Path to config file (default ~/.config/hotpot.toml)")]
    pub config: PathBuf,
    #[arg(short = 's', long, help = "Skip git fetch during startup", required = false)]
    pub skip_fetch: bool,
}

fn main() -> Result<()> {
    let args = Args::parse();
    let config = hotpot::config::load_config(&args.config)?;
    let git = HpGit::init(&config)?;
    let mut hpmqtt = HpMqtt::init(&config.host, config.port)?;

    loop {
        match hpmqtt.next_repo_state() {
            Ok(state) => {
                println!("Aports update! new HEAD: {}", state.commit);
                /* TODO:
                 * - pull aports (recording the previous HEAD)
                 * - pull pmaports
                 * - parse (and cache?) packages that are forked from aports
                 *   maybe call out to pmbootstrap for this??
                 * - compare the new aports HEAD with the previous HEAD for
                 *   affected packages
                 * - update pkgver/pkgrel, re-generate checksums and open MR
                 *   Probably use pmbootstrap for this...
                 */
            },
            Err(e) => {
                continue;
            }
        }
    }

    Ok(())
}
