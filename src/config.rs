use std::{os, path::PathBuf};

use serde_derive::Deserialize;
use toml;
use anyhow::Result;

#[derive(Deserialize)]
pub struct Config {
    pub host: String,
    pub port: u16,
    pub gitlab_token: String,
    pub signing_key: String,
    #[serde(default = "default_workdir")]
    pub workdir: PathBuf,

    pub aports_url: String,
    pub pmaports_url: String,
    pub pmaports_branch: String,
}

fn default_workdir() -> PathBuf {
    [dirs::home_dir().unwrap(), ".local/var/hotpot".into()].iter().collect()
}

pub fn load_config(path: &PathBuf) -> Result<Config> {
    let file = std::fs::File::open(path)?;
    let reader = std::io::BufReader::new(file);
    let contents = std::io::read_to_string(reader)?;
    let config = toml::from_str(&contents)?;
    Ok(config)
}
