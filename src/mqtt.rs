use std::time::Duration;
use serde_derive::Deserialize;

use rumqttc::{Client, ConnAck, Connection, Event, MqttOptions, Packet};
use anyhow::{bail, Result};

use crate::{config, RepoState};

#[derive(Deserialize)]
struct JsonBranchState {
    repo: String,
    head: String,
    #[serde(rename = "ref")]
    refname: String,
    user: String,
    size: u32,
    gitlab: bool,
}

pub struct HpMqtt {
    host: String,
    port: u16,
    client: Client,
    connection: Connection,
}

impl HpMqtt {
    pub fn init<S: Into<String>>(host: S, port: u16) -> Result<HpMqtt> {
        let host = host.into();
        let mqtt_options = MqttOptions::new("", &host, port)
            .set_keep_alive(Duration::from_secs(600))
            .set_clean_session(true)
            .to_owned();

        println!("[MQTT] Connecting to MQTT broker at {}:{}", &host, port);
        let (client, connection) = Client::new(mqtt_options, 10);

        Ok(HpMqtt { host, port, client, connection })
    }

    pub fn next_repo_state(&mut self) -> Result<RepoState> {
        let mut new_state = RepoState::default();

        /* wait for literally anything from the broker */
        let evt = match self.connection.recv() {
            Ok(maybe_evt) => {
                match maybe_evt {
                    Ok(evt) => evt,
                    Err(e) => {
                        println!("[MQTT] Connection error: {:?}", e);
                        return Err(e.into());
                    }
                }
            },
            Err(e) => {
                println!("[MQTT] Error receiving event: {:?}", e);
                bail!("Error receiving event");
            }
        };

        /* Handle incoming packets */
        if let Event::Incoming(p) = &evt {
            match p {
                /* ConAck is received immediately after connecting, at this point we subscribe
                 * to the topic
                 */
                Packet::ConnAck(_) => {
                    println!("[MQTT] Connected to broker at {}:{}", &self.host, self.port);
                    self.client.subscribe("git/aports/master", rumqttc::QoS::AtLeastOnce)?;
                },
                /* Confirm subscription */
                Packet::SubAck(_) => {
                    println!("[MQTT] Subscribed to topic 'git/aports/master'");
                },
                /* Receive actual data */
                Packet::Publish(p) => {
                    let payload = String::from_utf8_lossy(&p.payload);
                    println!("[MQTT] Received message on topic '{}': '{}'", p.topic, payload);
                    let state: JsonBranchState = serde_json::from_str(&payload)?;
                    new_state.commit = state.head;
                }
                p => {
                    println!("[MQTT] Received event: {:?}", p);
                },
            }
        }

        return Ok(new_state);
    }
}
